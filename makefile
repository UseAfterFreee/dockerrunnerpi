SRC_DIR=src
BIN_DIR=bin
OBJ_DIR=obj

SRC=$(wildcard $(SRC_DIR)/*.cpp)
OBJ=$(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(SRC))
UNIT_SRC=$(wildcard $(SRC_DIR)/*_unittest.cpp)
UNIT_OBJ=$(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(UNIT_SRC))
TEST_OBJ=$(filter-out $(OBJ_DIR)/main.o, $(OBJ))
RUN_OBJ=$(filter-out $(UNIT_OBJ), $(OBJ))

.PHONY: directories

all: directories $(BIN_DIR)/test $(BIN_DIR)/run

directories: $(BIN_DIR) $(OBJ_DIR)

$(BIN_DIR):
	mkdir -p $@

$(OBJ_DIR):
	mkdir -p $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	g++ -g -c -fprofile-arcs -ftest-coverage -o $@ $<

$(BIN_DIR)/test: $(TEST_OBJ)
	g++ -fprofile-arcs -ftest-coverage -o $@ $^ -lgtest -lgtest_main

$(BIN_DIR)/run: $(RUN_OBJ)
	g++ -fprofile-arcs -ftest-coverage -o $@ $^
