#include <gtest/gtest.h>
#include "math.h"
TEST(Factorial, Positive)
{
    EXPECT_EQ(factorial(1), 1);
    EXPECT_EQ(factorial(5), 120);
    EXPECT_EQ(factorial(10), 3628800);
}

TEST(Factorial, Zero)
{
    EXPECT_EQ(factorial(0), 1);
}

TEST(Factorial, Negative)
{
    EXPECT_EQ(factorial(-1), -1);
    EXPECT_EQ(factorial(-5), -1);
    EXPECT_EQ(factorial(-10), -1);
}
